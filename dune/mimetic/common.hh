// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_common.hh
#define DUNE_common.hh

#include <math.h>
#include <dune/common/fvector.hh>
// #include <dune/mimetic/integrateentity.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/mimetic/testfunctions.hh>
#include <dune/grid/io/file/vtk/function.hh>

template<typename ctype, class GV>
class PostPressure
{
public:
  typedef typename GV::template Codim< 0 >::EntityPointer EntityPointer;
  typedef Dune::BlockVector<Dune::FieldVector<ctype, 1> > ScalarField;
  typedef Dune::BlockVector<Dune::FieldVector<ctype, WORLDDIM> > VectorField;

  Dune::LeafMultipleCodimMultipleGeomTypeMapper<typename GV::Grid, Dune::MCMGElementLayout> elementmapper;
  PostPressure (const VectorField &g, const ScalarField &p, const typename GV::Grid &grid) :
    elementmapper(grid),_Kipostv(g), _currp(p) {}


  ctype operator() (const EntityPointer &entityptr, const Dune::FieldVector<ctype, WORLDDIM>& x) const
  {
    // Todo: Error checking?
    try {
      int elementindex = elementmapper.map(*entityptr);
      Dune::FieldVector<ctype, WORLDDIM> xc = entityptr->geometry().center();
      // return _currp[elementindex];
      return _currp[elementindex] - _Kipostv[elementindex]*(x - xc);
    }
    catch (Dune::Exception &e) {
      std::cerr << "Dune reported error: " << e << std::endl;
    }
  }

  typename VectorField::block_type getGradp(const EntityPointer &entityptr) const
  {
    int elementindex = elementmapper.map(*entityptr);
    typename VectorField::block_type ret(0.0);
    ret -= _Kipostv[elementindex];
    return ret;
  }

  // void delmem() {
  //   _Kipostv.~VectorField();
  //   _currp.~ScalarField();
  // }
private:
  // Use _postgradp instead of _postv when introducing diffusion constant K
  const VectorField _Kipostv;
  const ScalarField _currp;
};


template<int dimgrid>
struct FaceLayout
{
  bool contains (Dune::GeometryType gt)
  {
    if (gt.dim()==dimgrid - 1) return true;
    return false;
  }
};

    // class to write vector fields to VTK
template<typename GV, typename V>
class P0VTKVector
  : public Dune::VTKFunction< GV >
{
  //! Base class
  typedef Dune::VTKFunction< GV > Base;
  //! Mapper for elements
  typedef Dune::MultipleCodimMultipleGeomTypeMapper<GV, Dune::MCMGElementLayout> Mapper;

  //! store a reference to the vector
  const V& v;
  //! name of this function
  std::string s;
  //! number of components of the field stored in the vector
  int ncomps_;
  //! index of the component of the field in the vector this function is
  //! responsible for
  //! mapper used to map elements to indices
  Mapper mapper;

public:
  typedef typename Base::Entity Entity;
  typedef typename Base::ctype ctype;
  using Base::dim;

  //! return number of components
  virtual int ncomps () const
  {
    return ncomps_;
  }

  //! evaluate
  virtual double evaluate (int comp, const Entity& e,
                           const Dune::FieldVector<ctype,dim>& xi) const
  {
    return v[mapper.map(e)][comp];
  }

  //! get name
  virtual std::string name () const
  {
    return s;
  }


  P0VTKVector(const GV &gv, const V &v_, const std::string &s_,
              int ncomps)// , int mycomp)
    : v( v_ ),
      s( s_ ),
      ncomps_(ncomps),
      // mycomp_(mycomp),
      mapper( gv )
  {
    if (v.size()!=(unsigned int)(mapper.size()) || v[0].size() != ncomps_)
      DUNE_THROW(Dune::IOError, "P0VTKVector: size mismatch");
  }

  //! destructor
  virtual ~P0VTKVector() {}
};




#endif // Define DUNE_common.hh
