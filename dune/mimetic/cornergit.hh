// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_cornergit.hh
#define DUNE_cornergit.hh



/*******************************************************
 *  void cornergit(double &alpha,string &gridname)
 *  sets up a macro grid file for prolem injumping edge
 *
 *  the angle is double alpha
 *  In problem.hh you can find an adapted problem named
 *  ProblemCorner, well posed test for adaptive refinment
 *******************************************************/

void cornergit(double &alpha, std::string &gridname) {
  //Gitter fuer einspringende
  //Ecke erzeugen
  gridname = "corner.git";
  std::ofstream grid(gridname.c_str());
  grid << "DGF" << std::endl;
  grid << "Vertex" << std::endl;
  grid << "0 0" << std::endl;
  for (int i=0;i<5;++i)
    grid << cos(M_PI/180.*alpha*(double)(i)/4.) << " "
         << sin(M_PI/180.*alpha*(double)(i)/4.) << std::endl;
  grid << "#" << std::endl;
  if (0) {
    grid << "Simplex" << std::endl;
    for (int i=1;i<5;++i) {
      if (i%2)
        grid << 0 << " " << i << " " << i+1 << std::endl;
      else
        grid << i+1 << " " << 0 << " " << i << std::endl;
    }
    grid << "#" << std::endl;
  } else {
    grid << "Simplex" << std::endl;
    for (int i=1;i<5;++i)
      grid << i << " " << i+1 << " " << 0 << std::endl;
    grid << "#" << std::endl;
  }
  grid << "GridParameter" << std::endl;
  grid << "refinementedge longest" << std::endl;
  grid << "#" << std::endl;
  grid << "BoundarySegments" << std::endl;
  grid << 2 << " " << 0 << " " << 1 << std::endl;
  grid << 2 << " " << 0 << " " << 5 << std::endl;
  grid << "#" << std::endl;
  grid << "BoundaryDomain" << std::endl;
  grid << "Default 1" << std::endl;
  grid << "#" << std::endl;
  grid << "element boundaries:" << std::endl;
}

#endif // #define DUNE_cornergit.hh
