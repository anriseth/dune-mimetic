// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_matrixhelper.hh
#define DUNE_matrixhelper.hh



template< class K >
class MatrixHelper
{
public:
  template<int m, int n, int p>
  static Dune::FieldMatrix< K, m, p > MatTMultMat ( const Dune::FieldMatrix<K, n, m>& M1, \
                                                    const Dune::FieldMatrix<K, n, p>& M2 )
  {
    typedef typename Dune::FieldMatrix< K, m, p > :: size_type size_type;
    Dune::FieldMatrix<K, m, p> C(0.0);

    for (size_type i = 0; i < m; i++) {
      for (size_type j = 0; j < p; j++) {
        for (size_type k = 0; k < n; k++) {
          C[i][j] += M1[k][i] * M2[k][j];
        }
      }
    }
    return C;
  }

  template<int m, int n, int p>
  static Dune::FieldMatrix< K, m, p > MatMultMatT ( const Dune::FieldMatrix<K, m, n>& M1, \
                                                    const Dune::FieldMatrix<K, p, n>& M2 )
  {
    typedef typename Dune::FieldMatrix< K, m, p > :: size_type size_type;
    Dune::FieldMatrix<K, m, p> C(0.0);

    for (size_type i = 0; i < m; i++) {
      for (size_type j = 0; j < p; j++) {
        for (size_type k = 0; k < n; k++) {
          C[i][j] += M1[i][k] * M2[j][k];
        }
      }
    }
    return C;
  }

  // Not needed anymore
  template<int n>
  static Dune::FieldMatrix< K, n, n > IdentityMatrix ()
  {
    typedef typename Dune::FieldMatrix< K, n, n > :: size_type size_type;
    Dune::FieldMatrix<K,n,n> I(0.0);
    for (size_type i = 0; i < n; i++) {
      I[i][i] = 1.0;
    }
    return I;
  }
};

#endif // DUNE_matrixhelper.hh
