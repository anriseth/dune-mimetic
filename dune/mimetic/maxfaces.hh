// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

// A way to define the maximum number of faces for a conforming grid
// Is used by mimetic.hh to decide the sizes of the Blocks in the sparse matrices

#ifndef DUNE_maxfaces.hh
# define DUNE_maxfaces.hh

#ifndef MAXFACES
# define MAXFACES 3 // Default value
# if defined(ALBERTAGRID) || defined(ALUGRID_SIMPLEX) || defined(ALUGRID_CONFORM)
#  if GRIDDIM == 2
#   define MAXFACES 3
#  elif GRIDDIM == 3
#   define MAXFACES 4
#  endif
# endif
# if defined( SGRID) || defined(ALUGRID_CUBE)
#  if GRIDDIM == 2
#   define MAXFACES 4
#  elif GRIDDIM == 3
#   define MAXFACES 6
#  endif
# endif
#endif


#endif // define DUNE_maxfaces.hh
