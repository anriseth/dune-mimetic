// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_mimetic.hh
#define DUNE_mimetic.hh

#include <iostream>
#include <vector>
#include <set>
#include <cstdio>
#include <ctime>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/geometry/quadraturerules.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bdmatrix.hh>
#include <dune/istl/ilu.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>
#include <dune/istl/matrixmatrix.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>

// #define TIMELOGGER 1

#include <dune/mimetic/maxfaces.hh>
#include <dune/mimetic/common.hh>

 // TODO: Make checks on whether the grid is conforming or no ?
template<class GV>
class Mimetic
{

private:
  const GV& gv;
  const Problem<Dune::GridSelector::dimworld>& prob;

  Dune::LeafMultipleCodimMultipleGeomTypeMapper<typename GV::Grid, Dune::MCMGElementLayout> elementmapper;
  Dune::LeafMultipleCodimMultipleGeomTypeMapper<typename GV::Grid, FaceLayout> facemapper;


public:
  static const unsigned int dim = GV::dimension;
  static const unsigned int dimworld = GV::dimensionworld;

  typedef typename GV::template Codim<0>::Iterator LeafElementIterator;
  typedef typename GV::template Codim<1>::Iterator LeafFaceIterator;
  typedef typename GV::template Codim<0>::Geometry::JacobianInverseTransposed JacobianInverseTransposed;
  typedef typename GV::IntersectionIterator IntersectionIterator;
  typedef typename GV::IndexSet LeafIndexSet;
  typedef typename LeafElementIterator::Entity::Geometry LeafElementGeometry;
  typedef typename LeafFaceIterator::Entity::Geometry LeafFaceGeometry;
  typedef typename IntersectionIterator::Intersection::Geometry IntersectionGeometry;
  typedef typename GV::template Codim< 0 >::EntityPointer CellEntityPointer;
  typedef typename GV::template Codim< 1 >::EntityPointer FaceEntityPointer;
  typedef typename GV::ctype ctype;
  typedef Dune::Matrix<Dune::FieldMatrix<ctype, 1, 1> > DenseMatrix;
  typedef Dune::BCRSMatrix<Dune::FieldMatrix<ctype, 1, 1> > Matrix;
  typedef Dune::BDMatrix<Dune::FieldMatrix<ctype, MAXFACES, MAXFACES> > BlockDiagMatrix;
  typedef Dune::BDMatrix<Dune::FieldMatrix<ctype, 1, 1> > DiagonalMatrix;
  typedef Dune::BlockVector<Dune::FieldVector<ctype, 1> > ScalarField;

  Dune::BDMatrix<Dune::FieldMatrix<ctype, 1, MAXFACES>> McDiv; // M_c * DIV (= B in article)
  Dune::BDMatrix<Dune::FieldMatrix<ctype, 1, 1> > iMcDivGrad; // Diagonal (McDivGrad)^{-1} (= D^{-1} in article)
  Dune::BCRSMatrix<Dune::FieldMatrix<ctype, 1, MAXFACES> > McDivWf;
  Dune::BlockVector<Dune::FieldMatrix<ctype, MAXFACES, WORLDDIM> > R; // Matrix used to create inner product

  Matrix A;
  BlockDiagMatrix Wf; // M_F^{-1}facevolume * (facecenter[i] - cellcenter[i]);
  Dune::BCRSMatrix<Dune::FieldMatrix<ctype, 1, MAXFACES> > FaceMatrix;
  Dune::BCRSMatrix<Dune::FieldMatrix<ctype, MAXFACES, 1> > FaceMatrixT; // Dune::matMultTransposeMat is VERY SLOW, so create this one explicitly instead
  Dune::BlockVector<Dune::FieldVector<ctype, MAXFACES> > globalDirichlet;
  Dune::BlockVector<Dune::FieldVector<ctype, 1> > globalNeumann;

  ScalarField b;
  ScalarField Mcf;
  ScalarField lambda;
  ScalarField p;
  Dune::BlockVector<Dune::FieldVector<ctype, MAXFACES> > u;
  Dune::BlockVector<Dune::FieldVector<ctype, WORLDDIM> > postu;
  Dune::BlockVector<Dune::FieldVector<ctype, WORLDDIM> > Kipostu;

  std::vector< std::set<int> > CAdjacencyPattern;
  int numcfpairings;

  Mimetic(const GV& gv_, const Problem<dimworld>& prob_) :              \
    gv(gv_), prob(prob_) , elementmapper(gv_.grid()), facemapper(gv_.grid()) {}

  // store adjacency information in a vector of sets
  void determineAdjacencyPattern();
  // assemble stiffness matrix A and right side b
  void assembleMatrices();
  // solve Al = b for lambda
  void solve();
  void postprocess();
  // finally recover pressure on cells and the vector field u
  void recover();
  void runall();
    // ctype postpressure();
};

template<class GV>
void Mimetic<GV>::runall()
{
  determineAdjacencyPattern();
  assembleMatrices();
  solve();
  recover();
  postprocess();
}

template<class GV>
void Mimetic<GV>::determineAdjacencyPattern()
{
  int cfindex = 0; // running index for (c,f)
  const int NF = gv.size(1);
  CAdjacencyPattern.resize(NF);

  const LeafElementIterator itend = gv.template end<0>();
  for (LeafElementIterator it = gv.template begin<0>(); it != itend; ++it)
  {
    int elementindex = elementmapper.map(*it);

    const IntersectionIterator isend = gv.iend(*it);
    for (IntersectionIterator is = gv.ibegin(*it) ; is != isend ; ++is)
    {
      FaceEntityPointer faceptr = it->template subEntity<1>(is->indexInInside());

      int faceindex = facemapper.map(*faceptr);
      CAdjacencyPattern[faceindex].insert(elementindex);

      cfindex++;
    }
  }
  numcfpairings = cfindex;
}



template<class GV>
void Mimetic<GV>::assembleMatrices()
{
  int N = gv.size(0);
  int NF = gv.size(1);
  Dune::BDMatrix<Dune::FieldMatrix<ctype, MAXFACES, MAXFACES> > S(N);

  Dune::FieldMatrix<ctype, WORLDDIM, WORLDDIM> Kc; // Local diffusion tensor
  Dune::FieldMatrix<ctype, MAXFACES, MAXFACES> Wc; // Inverse inner product on vector-field
  Dune::FieldMatrix<ctype, MAXFACES, WORLDDIM > Rc;  // Helpers for Wc
  Dune::FieldMatrix<ctype, MAXFACES, WORLDDIM > barN; // barN = N_c * K_c^{-1}
  Dune::FieldMatrix<ctype, 1, MAXFACES> Bc;
  Dune::FieldVector<ctype, dimworld> cellcenter;
  Dune::FieldVector<ctype, MAXFACES> dirichlet;
  Dune::BlockVector<Dune::FieldVector<ctype, MAXFACES> > r(N);

  int elementindex;

  McDiv =  Dune::template BDMatrix<Dune::FieldMatrix<ctype, 1, MAXFACES> >(N);
  Wf = BlockDiagMatrix(N);
  b.resize(NF, false);
  b = 0.0;
  lambda.resize(b.N(), false);
  lambda = 0.0;
  Mcf.resize(N, false);
  R.resize(N, false);
  iMcDivGrad = Dune::BDMatrix<Dune::FieldMatrix<ctype, 1, 1> >(N);
  globalDirichlet.resize(N, false);
  globalDirichlet = 0.0;
  globalNeumann.resize(NF, false);
  globalNeumann = 0.0;

  FaceMatrix.setSize(NF, N, numcfpairings);
  FaceMatrix.setBuildMode(Dune::BCRSMatrix<Dune::FieldMatrix<ctype, 1, MAXFACES> >::random);
  FaceMatrixT.setSize(N, NF, N*MAXFACES);
  FaceMatrixT.setBuildMode(Dune::BCRSMatrix<Dune::FieldMatrix<ctype, MAXFACES, 1> >::random);

  for (int i = 0; i < NF; ++i)
  {
    FaceMatrix.setrowsize(i, CAdjacencyPattern[i].size());
  }
  FaceMatrix.endrowsizes();
  for (int i = 0; i < N; ++i)
  {
    FaceMatrixT.setrowsize(i, MAXFACES);
  }
  FaceMatrixT.endrowsizes();

  // set sparsity pattern of FaceMatrix with the information gained in determineAdjacentyPattern
  for (int i = 0; i < NF; ++i)
  {
    std::template set<int>::iterator setend = CAdjacencyPattern[i].end();
    for (std::template set<int>::iterator setit = CAdjacencyPattern[i].begin();
         setit != setend; ++setit)
    {
      FaceMatrix.addindex(i, *setit);
      FaceMatrixT.addindex(*setit, i);
    }
  }
  FaceMatrix.endindices();
  FaceMatrixT.endindices();

  const LeafElementIterator itend = gv.template end<0>();
  for (LeafElementIterator it = gv.template begin<0>(); it != itend; ++it)
  {
    Rc = 0.0; barN = 0.0; Bc = 0.0; dirichlet = 0.0; Kc = 0.0;
    cellcenter = it->geometry().center();
    elementindex = elementmapper.map(*it);
    LeafElementGeometry cgeo = it->geometry();
    const double cellvolume = cgeo.volume();
    const double ic2 = 1.0 / (cellvolume * cellvolume);
    Dune::GeometryType gt = it->type();

    const IntersectionIterator isend = gv.iend(*it);
    for (IntersectionIterator is = gv.ibegin(*it) ; is != isend ; ++is)
    {
      const IntersectionGeometry igeo = is->geometry();
      const double facevolume = igeo.volume();
      const Dune::FieldVector<ctype, dimworld> facecenter = igeo.center();

      FaceEntityPointer faceptr = it->template subEntity<1>(is->indexInInside());

      int faceindex = facemapper.map(*faceptr);
      const Dune::FieldVector<ctype, dimworld> faceouternormal = is->centerUnitOuterNormal();

      int localIndex = is->indexInInside();
      Bc[0][localIndex] = -facevolume;
      McDiv[elementindex][elementindex][0][localIndex] = facevolume;

      if (is->boundary() == true && prob.isneumann(facecenter) == false) {
        // Dune::FieldVector<ctype, MAXFACES> localDirichlet(0.0);
        Dune::GeometryType igt = is->type();
        ctype faceint = 0.0;
        const Dune::QuadratureRule<ctype,dim-1>& rule = Dune::QuadratureRules<ctype,dim-1>::rule(igt,3);
        for (typename Dune::QuadratureRule<ctype,dim-1>::const_iterator r = rule.begin();
             r != rule.end() ; ++r)
        {
          ctype weight = r->weight();
          ctype detjac = igeo.integrationElement(r->position());
          ctype fval = prob.dirichlet(igeo.global(r->position()));

          faceint += fval * weight * detjac;
        }
        dirichlet[localIndex] = faceint;
        // std::cout << "faceint = " << faceint << std::endl;
        // FaceDirichletT[elementindex][faceindex] = localDirichlet;
      }
      else {
      // if (is->boundary() != true) {
        Dune::FieldMatrix<ctype, 1, MAXFACES> localIndexVector(0.0);
        Dune::FieldMatrix<ctype, MAXFACES, 1> localIndexVectorT(0.0);
        // Dune::FieldMatrix<ctype, MAXFACES, 1> localFCT(0.0);
        localIndexVector[0][localIndex] = facevolume; // Should I use FieldVector instead for these?
        localIndexVectorT[localIndex][0] = facevolume; // Should I use FieldVector instead for these?
        //localFCT[localIndex][0] = facevolume;

        FaceMatrix[faceindex][elementindex] = localIndexVector;
        FaceMatrixT[elementindex][faceindex] = localIndexVectorT;
        // FCT[elementindex][faceindex] = localFCT;

        // Neumann boundary
        if (is->boundary() == true) {
          const Dune::GeometryType igt = is->type();
          ctype faceint = 0.0;
          const Dune::FieldVector<double, WORLDDIM> outernormal = is->centerUnitOuterNormal();
          const Dune::QuadratureRule<ctype,dim-1>& rule = Dune::QuadratureRules<ctype,dim-1>::rule(igt,3);
          for (typename Dune::QuadratureRule<ctype,dim-1>::const_iterator r = rule.begin();
               r != rule.end() ; ++r)
          {
            const ctype weight = r->weight();
            const ctype detjac = igeo.integrationElement(r->position());
            const ctype fval = prob.neumann(igeo.global(r->position()), outernormal);

            faceint += fval * weight * detjac;
          }
          globalNeumann[faceindex] = faceint; // I think we should have the facevolume for this one?
        }
      }

      for (int i = 0; i < dimworld; ++i)
      {
        barN[localIndex][i] = faceouternormal[i];
        Rc[localIndex][i] = facevolume * (facecenter[i] - cellcenter[i]);
      }
    }

    const Dune::QuadratureRule<ctype,dim>& rule = Dune::QuadratureRules<ctype,dim>::rule(gt,3);
    for (typename Dune::QuadratureRule<ctype,dim>::const_iterator r = rule.begin();
         r != rule.end() ; ++r)
    {
      const ctype weight = r->weight();
      const ctype detjac = cgeo.integrationElement(r->position());
      const ctype fval = prob.f(cgeo.global(r->position()));

      Mcf[elementindex] += fval * weight * detjac;

      const Dune::FieldMatrix<double, dim, dim> Kval = prob.K(cgeo.global(r->position()));

      for (int i = 0; i < dim; i++) {
        for (int j = i; j < dim; j++) {
          Kc[i][j] += Kval[i][j] * weight * detjac;
        }
      }
    }
    for (int i = 0; i < dim; i++) {
      for (int j = i; j < dim; j++) {
        Kc[i][j] *= ic2;
      }
    }
    for (int i = 1; i < dim; i++) {
      for (int j = 0; j<i; j++) {
        Kc[i][j] = Kc[j][i];
      }
    }


    // Kc *= cellvolume;

    Dune::FieldMatrix<ctype, MAXFACES, dim> barNKc = barN.rightmultiplyany(Kc);
    Wc = MatrixHelper<ctype>::MatMultMatT(barNKc, barN);
    double trace = 0.0;
    for (int i = 0; i < MAXFACES; ++i)
    {
      trace += Wc[i][i];
    }

    Dune::FieldMatrix<ctype, WORLDDIM, WORLDDIM> RtRi =
      MatrixHelper<ctype>::MatTMultMat(Rc, Rc);
    RtRi.invert();
    Dune::FieldMatrix<ctype, WORLDDIM, MAXFACES> tmp0 =
      MatrixHelper<ctype>::MatMultMatT(RtRi, Rc);
    Dune::FieldMatrix<ctype, MAXFACES, MAXFACES> tmp2 =
      tmp0.leftmultiplyany(Rc);
    tmp2 *= -1.0;
    for (int i = 0; i < MAXFACES; i++)
    {
      tmp2[i][i] += 1.0;
    }
    tmp2 *= 0.5 * trace;
    Wc += tmp2;
    // cellvolume cancels with Kci
    // Wc *= 1 / cellvolume;

    Wf[elementindex][elementindex] = Wc;

    r[elementindex] = 0.0;
    double D = 0.0;
    double BWdirichlet = 0.0;
    Dune::FieldVector<ctype, MAXFACES> Wdirichlet(0.0);
    for (int i = 0; i < MAXFACES; i++) {
      // (r[elementindex])[i] = 0.0;
      for (int j = 0; j < MAXFACES; j++) {
        D += Bc[0][i] * Wc[i][j] * Bc[0][j];
        Wdirichlet[i] += Wc[i][j] * dirichlet[j];
        BWdirichlet += Bc[0][i] * Wc[i][j] * dirichlet[j];
        (r[elementindex])[i] += Wc[i][j] * Bc[0][j];
      }
    }
    const double invD = 1.0 / D;
    iMcDivGrad[elementindex][elementindex] = invD;
    r[elementindex] *= invD * (BWdirichlet - Mcf[elementindex]);
    r[elementindex] -= Wdirichlet;

    Dune::FieldMatrix<ctype, MAXFACES, MAXFACES> tmp1 = MatrixHelper<ctype>::MatTMultMat(Bc, Bc);
    tmp2 = tmp1.rightmultiplyany(Wc);
    tmp2 *= -invD;
    for (int i = 0; i < MAXFACES; i++)
    {
      tmp2[i][i] += 1.0;
    }

    S[elementindex][elementindex] = tmp2.leftmultiplyany(Wc);
    // tmp1 = tmp2.leftmultiplyany(Wc);
    // S[elementindex][elementindex] = tmp1.rightmultiplyany(Fc);
    R[elementindex] = Rc;
    globalDirichlet[elementindex] = dirichlet;
  }

  // Dune::printSparseMatrix(std::cout, S, "S", "row", 4, 1);
  // Dune::printmatrix(std::cout, S[0][0], "S[0][0]", "row", 4, 1);
  Dune::BCRSMatrix<Dune::FieldMatrix<ctype, 1, MAXFACES> > CS;
  Dune::matMultMat(CS, FaceMatrix, S);
  Dune::matMultMat(A, CS, FaceMatrixT);
  // Dune::printSparseMatrix(std::cout, A, "A", "row", 4, 1);

  FaceMatrix.mv(r, b);
  b -= globalNeumann;

  // Enforce dirichlet in the system
  for ( LeafElementIterator it = gv.template begin<0>() ; it != itend ; ++it)
  {
    const IntersectionIterator isend = gv.iend(*it);
    for (IntersectionIterator is = gv.ibegin(*it) ; is != isend ; ++is)
    {
      const IntersectionGeometry igeo = is->geometry();
      const Dune::FieldVector<ctype, dimworld> facecenter = igeo.center();

      if ( is->boundary() == true && prob.isneumann(facecenter) == false )
      {
        FaceEntityPointer faceptr = it->template subEntity<1>(is->indexInInside());
        int faceindex = facemapper.map(*faceptr);
        elementindex = elementmapper.map(*it);
        int localIndex = is->indexInInside();

        A[faceindex] = 0.0;
        A[faceindex][faceindex] = 1.0;
        lambda[faceindex] = b[faceindex] = (globalDirichlet[elementindex])[localIndex];

      }
    }
  }

  // std::cout << "b.norm = " << b.two_norm()  << " A.frobnorm = " << A.frobenius_norm() <<std::endl;


}

template<class GV>
void Mimetic<GV>::solve()
{
  // make linear operator from A
  Dune::MatrixAdapter<Matrix,ScalarField,ScalarField> op(A);

  // initialize preconditioner
  Dune::SeqILUn<Matrix,ScalarField,ScalarField> ilu1(A, 1, 0.92);

  // the inverse operator
  // Dune::BiCGSTABSolver<ScalarField> bcgs(op, ilu1, 1e-15, 5000, 1);
  // Dune::CGSolver<ScalarField> cgs(op, ilu1, 1e-15, INT_MAX, 1);
  Dune::CGSolver<ScalarField> cgs(op, ilu1, 1e-15, gv.size(1), 0);
  Dune::InverseOperatorResult r;

  // finally solve the system
  // bcgs.apply(lambda, b, r);
  cgs.apply(lambda, b, r);
  A.~Matrix();
  // b.~ScalarField();
}

template<class GV>
void Mimetic<GV>::recover()
{
  p.resize(gv.size(0), false);

  Dune::BlockVector<Dune::FieldVector<ctype, MAXFACES> > McDivTp(gv.size(0)), Mfu, CTlambdaDirichlet(gv.size(0));
  ScalarField Dp(Mcf);

  FaceMatrixT.mv(lambda, CTlambdaDirichlet);
  CTlambdaDirichlet += globalDirichlet;

  Dune::matMultMat(McDivWf, McDiv, Wf);

  McDivWf.umv(CTlambdaDirichlet, Dp);
  iMcDivGrad.mv(Dp, p);

  u.resize(gv.size(0), false);
  McDiv.mtv(p, McDivTp);
  Mfu = McDivTp;
  Mfu -= CTlambdaDirichlet;
  Wf.mv(Mfu, u);

  // FCT.~BCRSMatrix();
  FaceMatrix.~BCRSMatrix();
  FaceMatrixT.~BCRSMatrix();
  Wf.~BDMatrix();
  McDiv.~BDMatrix();
  iMcDivGrad.~BDMatrix();
  McDivWf.~BCRSMatrix();

  // lambda.~BlockVector();
}

template<class GV>
void Mimetic<GV>::postprocess()
{
  postu.resize(gv.size(0));
  Kipostu.resize(gv.size(0));

  const LeafElementIterator itend = gv.template end<0>();
  for ( LeafElementIterator it = gv.template begin<0>() ; it != itend ; ++it)
  {
    Dune::GeometryType gt = it->type();
    int elementindex = elementmapper.map(*it);
    const LeafElementGeometry cgeo = it->geometry();
    double cellvolume = cgeo.volume(); /* Cancels out with Mc */

    Dune::FieldMatrix<ctype, dim, dim> Kci(0.0);
    const Dune::QuadratureRule<ctype,dim>& rule = Dune::QuadratureRules<ctype,dim>::rule(gt,3);
    for (typename Dune::QuadratureRule<ctype,dim>::const_iterator r = rule.begin();
         r != rule.end() ; ++r)
    {
      const ctype weight = r->weight();
      const ctype detjac = cgeo.integrationElement(r->position());

      const Dune::FieldMatrix<double, dim, dim> Kval = prob.K(cgeo.global(r->position()));
      for (int i = 0; i < dim; i++) {
        for (int j = i; j < dim; j++) {
          Kci[i][j] += Kval[i][j] * weight * detjac;
        }
      }
    }
    for (int i = 1; i < dim; i++) {
      for (int j = 0; j<i; j++) {
        Kci[i][j] = Kci[j][i];
      }
    }
    Kci.invert();

    R[elementindex].mtv(u[elementindex], postu[elementindex]);
    Kci.mv(postu[elementindex], Kipostu[elementindex]);

    postu[elementindex] /= cellvolume;
    // R.~BlockVector();
  }

}


#endif // DUNE_mimetic.hh
