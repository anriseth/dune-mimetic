// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_testfunctions.hh
#define DUNE_testfunctions.hh

#include <dune/common/fvector.hh>
#include <dune/mimetic/matrixhelper.hh>
#include <cmath>
#include <dune/mimetic/cornergit.hh>

template<int dim>
struct Problem
{
  typedef Dune::FieldVector<double, dim> GlobalCoordType;
  typedef Dune::FieldMatrix<double, dim, dim> Matrix;
  // typedef Dune::GridSelector::GridType GridType;
  virtual double f(const GlobalCoordType &x) const = 0;
  virtual double p(const GlobalCoordType &x) const = 0;
  virtual GlobalCoordType dp(const GlobalCoordType &x) const = 0;

  virtual double dirichlet(const GlobalCoordType &x) const
  {
    return p(x);
  }

  virtual double neumann(const GlobalCoordType &x, const GlobalCoordType &normal) const
  {
    GlobalCoordType Kdp(0.0);
    K(x).mv(dp(x), Kdp);
    return  -1.0 * Kdp.dot(normal);
    // return (K(x).mv(dp(x)).dot(normal));
  }

  /** \brief return true if section of boundary should use Neumann data.
   *    if false, we use Dirichlet data.
   */
  virtual bool isneumann(const GlobalCoordType &x) const
  {
    return false;
  }

  /** \brief return the diffusion tensor at position x
      \param[in] x coordinate to evaluate K at
      \return K(x)
  */
  virtual Matrix K(const GlobalCoordType &x) const
  {
    Matrix retval = MatrixHelper<double>::template IdentityMatrix<dim>();
    return retval;
  }

  /** \brief return the name of the grid file to use
   */
  virtual const std::string gridFileName() const
  {
    return "";
  }
  // GridType grid;
};

// Only written for 2D atm
template<int dim>
struct Test1 : public Problem<dim>
{
  typedef Dune::FieldVector<double, dim> GlobalCoordType;
  typedef Dune::FieldMatrix<double, dim, dim> Matrix;
  double f(const GlobalCoordType &x) const
  {
    double result = 0;
    for (int i=0 ; i < dim ; i++)
      result += x[i]* (1-x[i]);
    return 20.0 * result;
  }

  double p(const GlobalCoordType &x) const
  {
    double result = 1.0;
    for (int i=0 ; i < dim ; i++)
      result *= x[i]* (1.0-x[i]);
    return 10.0 * (result + 1.0);
  }

  GlobalCoordType dp(const GlobalCoordType &x) const
  {
    GlobalCoordType ret(0.0);

    ret[0] = 10.0*(1.0 - 2*x[0])* (x[1]-x[1]*x[1]);
    ret[1] = 10.0*(1.0 - 2*x[1])* (x[0]-x[0]*x[0]);

    return ret;
  }
};


template<int dim>
struct Test2 : public Problem<dim>
{
  typedef Dune::FieldVector<double, dim> GlobalCoordType;
  typedef Dune::FieldMatrix<double, dim, dim> Matrix;
  double f(const GlobalCoordType &x) const
  {
    return 2.0 * dim;
  }

  double p(const GlobalCoordType &x) const
  {
    double result = 0.0;
    for (int i=0 ; i < dim ; i++)
      result += x[i]* (1-x[i]);
    return result;
  }

  GlobalCoordType dp(const GlobalCoordType &x) const
  {
    GlobalCoordType ret;
    for (int i = 0; i < dim; i++) {
      ret[i] = 1.0 - 2.0*x[i];
    }
    return ret;
  }

  bool isneumann(const GlobalCoordType &x) const
  {
    if (x[0] == 1)
      return true;
    return false;
  }

};

template<int dim>
struct Test3 : public Problem<dim>
{
  typedef Dune::FieldVector<double, dim> GlobalCoordType;
  typedef Dune::FieldMatrix<double, dim, dim> Matrix;
  double f(const GlobalCoordType &x) const
  {
    return 2.0 * x[0] - 1.0;
  }

  double p(const GlobalCoordType &x) const
  {
    return x[0]*x[0]* ( 0.5 - x[0] / 3) + x[0] + x[1];
  }

  GlobalCoordType dp(const GlobalCoordType &x) const
  {
    GlobalCoordType ret;
    ret[0] = 1.0 + x[0] - x[0]*x[0];
    ret[1] = 1.0;
    return ret;
  }

  // double neumann(const GlobalCoordType &x) const
  // {
  //   if (x[0] == 0)
  //     return 1.0;
  //   else if (x[0] == 1)
  //     return -1.0;
  //   else
  //     return 0.0;
  // }

  bool isneumann(const GlobalCoordType &x) const
  {
    if ((x[0] == 0) || (x[0] == 1)) {
      return true;
    }
    return false;
  }
};


template<int dim>
struct Test4 : public Problem<dim>
{
  typedef Dune::FieldVector<double, dim> GlobalCoordType;
  typedef Dune::FieldMatrix<double, dim, dim> Matrix;
  double f(const GlobalCoordType &x) const
  {
    const double twopi = 2.0 * M_PI;
    double sx = sin(twopi*x[0]);
    double cx = cos(twopi*x[0]);
    double sy = sin(twopi*x[1]);
    double cy = cos(twopi*x[1]);
    return 8.0*M_PI*M_PI*sx*sy - 4.0*M_PI*M_PI*cx*cy;
  }

  double p(const GlobalCoordType &x) const
  {
    return sin(2.0*M_PI*x[0]) * sin(2.0*M_PI*x[1]);
  }

  GlobalCoordType dp(const GlobalCoordType &x) const
  {
    GlobalCoordType ret;
    double factor = 2.0*M_PI;
    ret[0] = factor * cos(factor*x[0]) * sin(factor*x[1]);
    ret[1] = factor * cos(factor*x[1]) * sin(factor*x[0]);
    return ret;
  }

  bool isneumann(const GlobalCoordType &x) const
  {
    if (x[0] < 0.5)
      return true;
    else
      return false;
  }

  Matrix K(const GlobalCoordType &x) const
  {
    Matrix retval = MatrixHelper<double>::template IdentityMatrix<dim>();

    retval[0][1] = 0.5;
    retval[1][0] = 0.5;
    return retval;
  }
};

template<int dim>
struct Test5 : public Problem<dim>
{
  typedef Dune::FieldVector<double, dim> GlobalCoordType;
  typedef Dune::FieldMatrix<double, dim, dim> Matrix;
  double f(const GlobalCoordType &x) const
  {
    double retval = ((x[0]+1)*(x[0]+1)+x[1]*x[1])*(2-4*M_PI*M_PI*sin(2*M_PI*x[0])*sin(2*M_PI*x[1])) +
      (x[0]+1)*(x[0]+1)*(2-4*M_PI*M_PI*sin(2*M_PI*x[0])*sin(2*M_PI*x[1])) -
      8*M_PI*M_PI*x[0]*x[1]*cos(2*M_PI*x[0])*cos(2*M_PI*x[1]) +
      2*(x[0]+1)*(2*M_PI*cos(2*M_PI*x[0])*sin(2*M_PI*x[1])+2*x[0]) -
      x[1]*(2*M_PI*sin(2*M_PI*x[0])*cos(2*M_PI*x[1])+2*x[1]) -
      x[0]*(2*M_PI*cos(2*M_PI*x[0])*sin(2*M_PI*x[1])+2*x[0]);

    return -retval;
  }

  double p(const GlobalCoordType &x) const
  {
    return sin(2*M_PI*x[0])*sin(2*M_PI*x[1]) + x[0]*x[0] + x[1]*x[1] + 1.0;
  }

  GlobalCoordType dp(const GlobalCoordType &x) const
  {
    GlobalCoordType ret;
    const double factor = 2.0 * M_PI;
    ret[0] = factor*cos(factor*x[0])*sin(factor*x[1]) + 2.0*x[0];
    ret[1] = factor*cos(factor*x[1])*sin(factor*x[0]) + 2.0*x[1];
    return ret;
  }


  Matrix K(const GlobalCoordType &x) const
  {
    Matrix retval;
    retval[0][0] = (x[0]+1.0)*(x[0]+1.0) + x[1]*x[1];
    retval[1][1] = (x[0]+1.0)*(x[0]+1.0);
    retval[0][1] = -x[0]*x[1];
    retval[1][0] = retval[0][1];
    return retval;
  }
};


// This is the same both with and without K
template<int dim>
struct Test6 : public Problem<dim>
{
  typedef Dune::FieldVector<double, dim> GlobalCoordType;
  typedef Dune::FieldMatrix<double, dim, dim> Matrix;
  double f(const GlobalCoordType &x) const
  {
    return -4.0;
  }

  double p(const GlobalCoordType &x) const
  {
    return (x[0]-0.5)*(x[0]-0.5) +(x[1]-0.5)*(x[1]-0.5);
  }

  GlobalCoordType dp(const GlobalCoordType &x) const
  {
    GlobalCoordType ret;
    ret[0] = 2.0*(x[0]-0.5);
    ret[1] = 2.0*(x[1]-0.5);
    return ret;
  }

  Matrix K(const GlobalCoordType &x) const
  {
    Matrix retval = MatrixHelper<double>::template IdentityMatrix<dim>();

    retval[0][1] = 0.5;
    retval[1][0] = 0.5;
    return retval;
  }
};

/** \brief Implementation of Problem for the injumping Edge
    \param[in] alpha_grad the angle of injumping edge
 */
class ProblemCorner : public Problem<WORLDDIM>
{
  typedef Dune::FieldVector<double, WORLDDIM> GlobalCoordType;
  typedef Dune::FieldMatrix<double, WORLDDIM, WORLDDIM> Matrix;

  double lambda_;
  public:
  /** \brief constructor
   * \param[in] alpha_grad angle for domain in grad
   */
  ProblemCorner(double angle)
  {
    if ( angle <= 0.0 || angle > 360.0 )
    {
      std::cerr << "the corner problem requires an additional parameter for "
                << "  the angle between >0 and <=360."
                << std::endl;
      std::abort();
    }
    std::cout << "Corner problem with angle=" << angle << std::endl;
    lambda_ = 180.0/angle;
    cornergit(angle,gridname_);
  }
  virtual ~ProblemCorner(){}

  /** \brief 0 right hand side
   */
  double f(const GlobalCoordType &x) const
  {
    return 0.0 ;
  }

  /** \brief u as exact solution
   */
  double p(const GlobalCoordType &x) const
  {
    double r2 = radius(x[0],x[1]);
    double phi = argphi(x[0],x[1]);
    return pow(r2,lambda_*0.5)*sin(lambda_*phi);
  }

  /** \brief derivative of u for H2 norm
   */
  GlobalCoordType dp(const GlobalCoordType &x) const
  {
    GlobalCoordType d;

    double r2=radius(x[0],x[1]);
    double phi=argphi(x[0],x[1]);
    double r2dx=2.*x[0];
    double r2dy=2.*x[1];
    double phidx=-x[1]/r2;
    double phidy=x[0]/r2;
    double lambdaPow = (lambda_*0.5)*pow(r2,lambda_*0.5-1.);
    d[0]= lambdaPow * r2dx * sin(lambda_ * phi)
          + lambda_ * cos( lambda_ * phi) * phidx * pow(r2,lambda_ * 0.5);
    d[1]= lambdaPow * r2dy * sin(lambda_ * phi)
          + lambda_ * cos( lambda_ * phi) * phidy * pow(r2,lambda_*0.5);
    assert( d[0] == d[0] );
    assert( d[1] == d[1] );

    return d;
  }
  /** \brief return the name of the grid file to use
   */
  virtual const std::string gridFileName() const
  {
    return gridname_;
  }

  private:
  /** \brief proper implementation of atan(x,y)
   */
  inline double argphi(double x,double y) const
  {
    double phi= arg(std::complex<double>(x,y));
    if (y<0) phi+=2.*M_PI;
    return phi;
  }

  /** \brief implementation for the radius squared
   * (^0.5 is part of function implementation)
   */
  inline double radius(double x, double y) const
  {
    double ret =0;
    ret = x*x +y*y;
    return ret;
  }
  std::string gridname_;
};


template<int dim>
class ProblemC1 : public Problem<dim> {
  typedef Dune::FieldVector<double, dim> GlobalCoordType;

public:
  ProblemC1() {}
  virtual ~ProblemC1() {}

  double p(const GlobalCoordType &x) const
  {
    double r2 = x.two_norm2();
    return (r2<0.5)?cos(r2*2.*M_PI)+1.:0;
  }

  GlobalCoordType dp(const GlobalCoordType &x) const
  {
    GlobalCoordType d;

    double r2 = x.two_norm2();
    double factor = (r2<0.5)?-4.*M_PI*sin(r2*2.*M_PI):0;
    d[0] = factor*x[0];
    d[1] = factor*x[1];

    return d;
  }

  // not needed yet
  double f(const GlobalCoordType &x) const
  {
    double r2 = x.two_norm2();
    if (r2>0.5)
      return 0.0;
    else
    {
      double f1=r2*16.*M_PI*M_PI*cos(r2*2.*M_PI);
      double f2=8.*M_PI*sin(r2*2.*M_PI);
      return f1+f2;
    }
  }
};


/** \brief Problem for diffusion equation from Benchmark 5
 * http://www.latp.univ-mrs.fr/latp_numerique/?q=node/18
 */
class ProblemBenchmark5 : public Problem<WORLDDIM> {
  typedef Dune::FieldVector<double, WORLDDIM> GlobalCoordType;
  typedef Dune::FieldMatrix<double, WORLDDIM, WORLDDIM> Matrix;

public:
  ProblemBenchmark5() :
    delta_(1e-3)
  {
  }
  virtual ~ProblemBenchmark5() {}

  // unkown
  double p(const GlobalCoordType &x) const
  {
    return sin(M_PI*x[0])*sin(M_PI*x[1]);
  }

  GlobalCoordType dp(const GlobalCoordType &x) const
  {
    GlobalCoordType d;

    d[0] = M_PI * cos(M_PI*x[0])*sin(M_PI*x[1]);
    d[1] = M_PI * cos(M_PI*x[1])*sin(M_PI*x[0]);
    return d;
  }


  double dirichlet(const GlobalCoordType &x) const
  {
    return 0.0;
  }

  double f(const GlobalCoordType &point) const
  {
    double k[2][2];
    Kpriv(point,k);
    double x = point[0];
    double y = point[1];
    double rt = x*x+y*y;
    double ux = M_PI * cos(M_PI*x)*sin(M_PI*y);
    double uy = M_PI * cos(M_PI*y)*sin(M_PI*x);
    double f0 = sin(M_PI*x)*sin(M_PI*y)*M_PI*M_PI*(1+delta_)*(x*x+y*y)
      + cos(M_PI*x)*sin(M_PI*y)*M_PI*(1.-3.*delta_)*x
      + cos(M_PI*y)*sin(M_PI*x)*M_PI*(1.-3.*delta_)*y
      + cos(M_PI*y)*cos(M_PI*x)*2.*M_PI*M_PI*(1.-delta_)*x*y;
    double kxx = k[0][0];
    double kyy = k[1][1];
    double kxy = k[0][1];
    return (f0+2.*(x*(kxx*ux+kxy*uy)+y*(kxy*ux+kyy*uy)))/rt;
  }

  Matrix K(const GlobalCoordType &arg) const
  {
    Matrix k;
    double kpriv[2][2];
    Kpriv(arg, kpriv);
    k[0][0] = kpriv[0][0];
    k[1][1] = kpriv[1][1];
    k[0][1] = k[1][0] = kpriv[0][1];

    return k;
  }


private:
  void Kpriv(const GlobalCoordType &arg, double (&k)[2][2] ) const
  {
    double x = arg[0];
    double y = arg[1];
    double rt = x*x+y*y;
    k[0][0] = (y*y+delta_*x*x)/rt;
    k[1][1] = (x*x+delta_*y*y)/rt;
    k[1][0] = k[0][1] = -(1.0-delta_)*x*y/rt;
  }
  void dK_dx(const GlobalCoordType &arg, double (&dxk)[2][2] ) const
  {
    double k[2][2];
    Kpriv(arg,k);
    double x = arg[0];
    double y = arg[1];
    double rt = x*x+y*y;
    dxk[0][0] = 2.*x/rt*(delta_-k[0][0]);
    dxk[1][1] = 2.*x/rt*(1.-k[1][1]);
    dxk[1][0] = dxk[0][1] = -(1.-delta_)*y/rt*(y*y-x*x);
  }
  void dK_dy(const GlobalCoordType &arg, double (&dyk)[2][2] ) const
  {
    double k[2][2];
    Kpriv(arg,k);
    double x = arg[0];
    double y = arg[1];
    double rt = x*x+y*y;
    dyk[0][0] = 2.*y/rt*(1.-k[0][0]);
    dyk[1][1] = 2.*y/rt*(delta_-k[1][1]);
    dyk[1][0] = dyk[0][1] = -(1.-delta_)*x/rt*(x*x-y*y);
  }

  const double delta_;
};


/** \brief Problem for diffusion equation from Benchmark 3
 * http://www.latp.univ-mrs.fr/latp_numerique/?q=node/16
 */
class ProblemBenchmark3 : public Problem<WORLDDIM> {
  typedef Dune::FieldVector<double, WORLDDIM> GlobalCoordType;
  typedef Dune::FieldMatrix<double, WORLDDIM, WORLDDIM> Matrix;

  public:
    ProblemBenchmark3()
      : globalShift_(0.0)
      , delta_(1e-3)
      // , pi_ ( 4. * atan(1.) )
      , pi_ ( M_PI)
      , cost_ ( cos( 40. * pi_ / 180. ) )
      , sint_ ( sqrt(1. - cost_*cost_) )
    {
      factor_[0][0] = cost_*cost_+delta_*sint_*sint_;
      factor_[1][0] = factor_[0][1] = cost_*sint_*(1.-delta_);
      factor_[1][1] = sint_*sint_+delta_*cost_*cost_;
    }
    virtual ~ProblemBenchmark3() {}

  // no analytical solution, set to zero?
    double p(const GlobalCoordType &x) const
    {
      return 0.;
    }

    // no analytical solution, set to zero?
    GlobalCoordType dp(const GlobalCoordType &x) const
    {
      GlobalCoordType ret(0.0);
      return ret;
    }

    // set to zero
    double f(const GlobalCoordType &point) const
    {
      return 0.;
    }


    double dirichlet(const GlobalCoordType &x) const
    {
      const int bndId = getBoundaryId( x );
      if( bndId == 0 )
        return bndFunc(x[1],0.2,0.3,1,0.5);
      else if( bndId == 1 )
        return bndFunc(x[1],0.7,0.8,0.5,0);
      else if( bndId == 2 )
        return bndFunc(x[0],0.2,0.3,1,0.5);
      else if( bndId == 3 )
        return bndFunc(x[0],0.7,0.8,0.5,0);
      return 0.5;
    }
    /** \brief return the name of the grid file to use
     */
    virtual const std::string gridFileName() const
    {
      return "../problem/cube.dgf";
    }

  Matrix K(const GlobalCoordType &arg ) const
  {
    Matrix k;
    double kpriv[2][2];
    Kpriv(arg, kpriv);
    k[0][0] = kpriv[0][0];
    k[1][1] = kpriv[1][1];
    k[0][1] = k[1][0] = kpriv[0][1];

    return k;
  }


  private:
    void Kpriv(const GlobalCoordType &arg, double (&k)[2][2] ) const
    {
      for(int i=0; i<2; ++i)
      {
        k[i][i] = factor_[i][i];
        for(int j=0; j<i; ++j)   k[i][j] = factor_[i][j];
        for(int j=i+1; j<2; ++j) k[i][j] = factor_[i][j];
      }
    }
    double bndFunc(double x, double lower, double upper, double lowval, double upval) const
    {
      if( x <= lower ) return lowval;
      if( x >= upper ) return upval;
      const double scale = (x - lower)/(upper - lower);
      assert( scale >= 0 && scale <= 1 );
      return (1. - scale) * lowval + scale * upval;
    }
    int getBoundaryId(const GlobalCoordType &x) const
    {
      if (std::abs(x[0]) < 1e-8) return 0;
      else if (std::abs(x[1]) < 1e-8) return 2;
      else if (std::abs(x[0]-1.) < 1e-8) return 1;
      else if (std::abs(x[1]-1.) < 1e-8) return 3;
      abort();
      return 0;
    }

    const double globalShift_;
    double factor_[2][2];
    const double delta_;
    const double pi_;
    const double cost_;
    const double sint_;
};



template<int dim>
struct ProblemExp : public Problem<dim>
{
  typedef Dune::FieldVector<double, dim> GlobalCoordType;
  typedef Dune::FieldMatrix<double, dim, dim> Matrix;

  ProblemExp() : delta_(1e-2) {}

  double f(const GlobalCoordType &x) const
  {
    const Matrix k = K(x);
    const Matrix dKx = dK_dx(x);
    const Matrix dKy = dK_dy(x);
    GlobalCoordType Kx;
    k.mv(x, Kx);
    const double f0 = -2.0* exp(x.two_norm2());
    const double dKpx = k[0][0] + dKx[0][0]*x[0] + dKx[0][1]*x[1];
    const double dKpy = k[1][1] + dKy[1][0]*x[0] + dKy[1][1]*x[1];

    return f0 * (2.0*x[0]*Kx[0] + 2.0*x[1]*Kx[1] + dKpx + dKpy);
  }

  double p(const GlobalCoordType &x) const
  {
    return exp(x.two_norm2());
  }

  GlobalCoordType dp(const GlobalCoordType &x) const
  {
    GlobalCoordType ret;
    const double tux = 2.0 * p(x);
    ret[0] = x[0] * tux;
    ret[1] = x[1] * tux;

    return ret;
  }

  Matrix K(const GlobalCoordType &x) const
  {
    Matrix k =  MatrixHelper<double>::template IdentityMatrix<dim>();;
    // k[0][0] = 1.0;
    // k[0][1] = k[1][0] = 0.0;
    k[1][1] = x.two_norm2() + delta_;

    return k;
  }

  bool isneumann(const GlobalCoordType &x) const
  {
    if (x.two_norm2() > 1.2)
      return true;
    return false;
  }


private:
  Matrix dK_dx(const GlobalCoordType &x) const
  {
    Matrix dK(0.0);
    dK[1][1] = 2.0 * x[0];
    return dK;
  }


  Matrix dK_dy(const GlobalCoordType &x) const
  {
    Matrix dK(0.0);
    dK[1][1] = 2.0 * x[1];
    return dK;
  }

  const double delta_;
};


#endif // DUNE_testfunctions.hh
