// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2; compilation-skip-threshold 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <dune/grid/utility/gridtype.hh>

// #include <stdio.h>
// #include <pthread.h> //Sleep
#include <unistd.h> // Sleep

#include <sstream>
#include <iostream>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/grid/io/file/vtk/vtkwriter.hh>


// Grid includes
#include <dune/grid/sgrid.hh>
#include <dune/grid/common/gridinfo.hh>
// #include <dune/grid/albertagrid.hh>
// #include <dune/grid/albertagrid/dgfparser.hh>

#include <dune/grid/io/file/dgfparser/dgfparser.hh>
#include <dune/grid/io/file/dgfparser.hh>
#include <dune/grid/io/file/dgfparser/gridptr.hh>


#include <dune/mimetic/mimetic.hh>
#include <dune/mimetic/testfunctions.hh>
#include <dune/grid/io/file/vtk/function.hh>

#define TIMELOGGER 1

//////////////////////////////////////////////////////////////
//
// calculate the error of between the exact solution and a
// linear discrete function
//
//////////////////////////////////////////////////////////////
template<class GV>
void error(const GV& gv,
           const Problem<GV::dimensionworld>& problem,
           const PostPressure<typename GV::ctype, GV> &postp,
           // const DiscreteFunctionType &uh,
           double &l2error,
           double &h1error)

{
  typedef typename GV::ctype ctype;
  const int dim = GV::dimensionworld;
  l2error = 0.0;
  h1error = 0.0;
  typedef typename GV::template Codim<0>::Iterator LeafElementIterator;
  typedef typename LeafElementIterator::Entity::Geometry LeafElementGeometry;
  // loop over all elements
  const LeafElementIterator itend = gv.template end<0>();
  for (LeafElementIterator it = gv.template begin<0>(); it != itend; ++it)
  {
    LeafElementGeometry cgeo = it->geometry();
    double locl2error=0.0;
    double loch1error=0.0;
    Dune::GeometryType gt = it->type();

    const Dune::FieldVector<double, WORLDDIM> gradp = postp.getGradp(*it);

    const Dune::QuadratureRule<ctype,dim>& rule = Dune::QuadratureRules<ctype,dim>::rule(gt,5);
    for (typename Dune::QuadratureRule<ctype,dim>::const_iterator r = rule.begin();
       r != rule.end() ; ++r)
    {
      const ctype weight = r->weight();
      const ctype detjac = cgeo.integrationElement(r->position());
      const Dune::FieldVector<double, WORLDDIM> x = cgeo.global(r->position());
      // const ctype fval = problem.f(cgeo.global(r->position()));

      // global coordinate of quadrature point
      // GlobalCoordType x = element.global( quad.point(qp) );

      // u( x )
      double locError = problem.p(x);

      locError -= postp(*it, x);
      // u_h( x )
      // locError -=  uh.evaluate( element, quad.point( qp ) );

      // add to local L2 error
      locl2error += weight * detjac * (locError * locError);

      // // grad u ( x )
      // GlobalCoordType du;
      // problem.du( x, du );

      // // minus grad u_h ( x )
      // du -= uh.gradient( element, quad.point(qp) ) ;

      // // H1-Fehler
      // loch1error += quad.weight(qp) * element.integrationElement()
      //               * ( du * du ) ;
      Dune::FieldVector<double, WORLDDIM> graderror = problem.dp(x);
      graderror -= gradp;

      loch1error += weight * detjac * graderror.two_norm2();
    }

    l2error += locl2error;
    h1error += loch1error;
  }
  l2error = sqrt(l2error);
  h1error = sqrt(h1error);
}


template<class GV>
double meshDiameter(const GV &gv)
{
  double h = 0.0;
  double hnew = 0.0;
  typedef typename GV::template Codim<0>::Iterator LeafElementIterator;
  typedef typename LeafElementIterator::Entity::Geometry LeafElementGeometry;
  // loop over all elements
  const LeafElementIterator itend = gv.template end<0>();
  for (LeafElementIterator it = gv.template begin<0>(); it != itend; ++it)
  {
    const LeafElementGeometry geo = it->geometry();
    const Dune::FieldVector<double, GV::dimensionworld> x0 = geo.corner(0);
    Dune::FieldVector<double, GV::dimensionworld> xold = geo.corner(1);

    hnew = (x0 - xold).two_norm();
    if (hnew > h)
      h = hnew;

    for (int i = 2; i < geo.corners(); i++) {
      Dune::FieldVector<double, GV::dimensionworld> xnew = geo.corner(i);
      hnew = (xold - xnew).two_norm();
      if (hnew > h)
        h = hnew;
      xold = xnew;
    }

    hnew = (x0 - xold).two_norm();
    if (hnew > h)
      h = hnew;

  }
  return h;
}

template<class G>
void compute(G& grid,
             const Problem<G::dimensionworld>& problem,
             const int refines)
{
  // const int dimension = grid.dimension;
  typedef typename G::LeafGridView GV;
  GV gv = grid.leafGridView();

  double l2error[refines];
  double h1error[refines];
  double eocl2[refines];
  double eoch1[refines];

  // double hold=1./sqrt(grid.size(dimension));

  double h = meshDiameter(gv);
  const int refperstep = Dune::DGFGridInfo<G>::refineStepsForHalf();

  // start computation
  for (int n=0; n < refines; ++n)
  {
    // std::cout << "sleeping ... " <<std::endl;
    // sleep(5);
    // std::cout << "done" <<std ::endl;
    // double hnew=1./sqrt(grid.size(dimension));

    // approximate solution
    // DiscreteFunctionType uh(grid);

    // compute approximate solution
    // interpolate(grid,problem,uh);
    // l2projection(grid,problem,uh);
    Mimetic<GV> mimetic(gv, problem);
    mimetic.runall();
    PostPressure<double, GV> postp(mimetic.Kipostu, mimetic.p, grid);

    // compute the approximation error
    error( gv, problem, postp, l2error[n], h1error[n] );


    if (n>0)
    {
      // const double hfraction=hold/hnew;
      const double hfraction = 2.0;
      eocl2[n]=(log(l2error[n-1])-log(l2error[n]))/
                log(hfraction);

      eoch1[n]=(log(h1error[n-1])-log(h1error[n]))/
                log(hfraction);
    }
    else
    {
      eocl2[0]=-1;
      eoch1[0]=-1;
    }

    std::cout << grid.size(0) << "\t  "
         << h << " "
         << " \t\t "
         << l2error[n] << " \t " << eocl2[n]
         << " \t\t "
         << h1error[n] << " \t " << eoch1[n]
         << " \t\t "
         << std::endl;

    // new h is old h now
    // hold = hnew;

    // optput data
    // Output output( grid, n );btestf

    // output.add( uh );
    // output.write();
    std::stringstream filename;
    filename << "mimetic_" << n;
    // if (WORLDDIM == 2) {
      Dune::VTKWriter<GV> vtkwriter(gv);
      vtkwriter.addCellData(mimetic.p, "p");
      // Dune::P0VTKVector
      typedef Dune::BlockVector<Dune::FieldVector<double, WORLDDIM> > vectorfield;
      typedef P0VTKVector<GV, vectorfield> VTKFunction;
      // VTKFunction test = VTKFunction(grid.leafView(), mimetic.postu, "u", WORLDDIM);
      // vtkwriter.addCellData(&test);
      vtkwriter.addCellData(Dune::shared_ptr<VTKFunction>(new VTKFunction(grid.leafView(), mimetic.postu, "u", WORLDDIM)));

      vtkwriter.write(filename.str(), Dune::VTK::appendedraw);
    // }
    // refine all elements of grid twice to half grid width
    //grid.globalRefine( GV :: dimension );
//     int refperstep = 2;
// #ifdef ALUGRID_CUBE
//     refperstep = 1;
// #endif
    grid.globalRefine( refperstep );
    h  *= 0.5;
    // postp.delmem();
  } // end of loop for (n=..)
}


int main(int argc, char** argv)
{

  try {

    typedef Dune::GridSelector::GridType GridType;
    // const int dim = GridType::dimension;
    // const int worlddim = GridType::dimensionworld;

    // read in command line parameters
    std::string gridname ( "../grids/test1.dgf" );
    int loops = 1;
    int probnum = 1;

    if (argc<4)
    {
      std::cout << "Usage: "<< argv[0] << " <grid file> <loops> <problem> " << std::endl;
      std::cout << "                         loops > 0" << std::endl;
      std::cout << "                         problem = 1..3" << std::endl;
    }
    else
    {
      gridname  = argv[1];
      loops    = atoi( argv[2] );
      probnum   = atoi( argv[3] );
    }

    Problem<WORLDDIM> *problem=0;
    switch (probnum)
    {
    case 1: problem = new Test1<WORLDDIM>();
      break;
    case 2: problem = new Test2<WORLDDIM>();
      break;
    case 3: problem = new Test3<WORLDDIM>();
      break;
    case 4: problem = new Test4<WORLDDIM>();
      break;
    case 5: problem = new Test5<WORLDDIM>();
      break;
    case 6: problem = new Test6<WORLDDIM>();
      break;
    case 7: problem = new ProblemCorner(270);
      gridname = problem->gridFileName();
      break;
    case 8: problem = new ProblemCorner(360);
      gridname = problem->gridFileName();
      break;
    case 9: problem = new ProblemCorner(40);
      gridname = problem->gridFileName();
      break;
    case 10: problem = new ProblemC1<WORLDDIM>();
      break;
    case 11:
      assert(WORLDDIM == 2);
      problem = new ProblemBenchmark5();
      break;
    case 12:
      assert(WORLDDIM == 2);
      problem = new ProblemBenchmark3();
      break;
    case 13:
      assert(WORLDDIM == 2);
      problem = new ProblemExp<WORLDDIM>();
      break;
    default: std::cerr << "Wrong problem number " << probnum
                       << ". Should be less than 13." << std::endl;
      return 1;
    }
    Dune::GridPtr<GridType> gridPtr(gridname.c_str());
    GridType & grid = *gridPtr;

    typedef typename GridType::LeafGridView GV;
    GV gv = grid.leafGridView();

    // grid.globalRefine(loops);
    // Mimetic<GV> mimetic(gv, *problem);
    // mimetic.runall();
    // for (int i = 0; i < 10; ++i)
    // {
    //   std::cout << "lambda[i] = " << mimetic.lambda[i] << std::endl;
    // }
    // return 1;

    const double h0 = 0.25;
    const double currh = meshDiameter(gv);
    if (currh > h0)
    {
      const int stepnum = floor(std::log(currh/h0) / std::log(2.0));
      const int refperstep = Dune::DGFGridInfo<GridType>::refineStepsForHalf();
      grid.globalRefine(refperstep*stepnum + 1);
    }
    compute( grid, *problem, loops );

    return 0;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
